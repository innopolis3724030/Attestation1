# -*- coding: utf-8 -*-
import re, os, csv, array
import aiohttp
import asyncio
import time

start_time = time.time()
class CityWeatherForecast:
    def __init__(self, n, t, h, p):
        self.city_name = n
        self.city_temp = t
        self.city_humidity = h
        self.city_pressure = p
    def showweatherforecast(self):
        print(f'Город:\t{self.city_name}')
        print(f'Температура: {self.city_temp}C°')
        print(f'Влажность: {self.city_humidity}%')
        print(f'Давление: {self.city_pressure}мм р.т.')
        print("")

f = open("cities.txt", "r", encoding='utf-8')


citiesstring = f.read()

result = re.findall(r'\b[a-zA-Z]+\b(?=.\W)|\b[a-zA-Z]+\b.\b[a-zA-Z]+\b(?=.\W)|\b[a-zA-Z]+\b(?=.\d)',citiesstring)

forecasts = []

async def Cityweatherrequest(cityname, array):
    openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
    async with aiohttp.ClientSession() as session:
        async with session.get(url=f'https://api.openweathermap.org/data/2.5/weather?q={cityname}&appid={openweatherAPI}&units=metric') as response:
            response_json = await response.read()
            response = await session.get(url=f'https://api.openweathermap.org/data/2.5/weather?q={cityname}&appid={openweatherAPI}&units=metric')
            response_text = await response.text()
            city_name = re.search(r'name":"[a-zA-Z -]+', response_text).group(0).split(":")[1].replace('"', "")
            city_temp = re.search(r'temp":\d+.\d+', response_text).group(0).split(":")[1]
            city_humidity = re.search(r'humidity":\d+', response_text).group(0).split(":")[1]
            city_pressure = re.search(r'pressure":\d+', response_text).group(0).split(":")[1]
            cityforecast = CityWeatherForecast(city_name, city_temp, city_humidity, city_pressure)
            array.append(cityforecast)

async def main(result):
    tasks = []
    for i in result:
        task = asyncio.create_task(Cityweatherrequest(i,forecasts))
        tasks.append(task)
    await asyncio.gather(*tasks)


asyncio.run(main(result))

for element in forecasts:
    element.showweatherforecast()

finish_time = time.time() - start_time
print(f"Затраченное на работу скрипта время: {finish_time}")